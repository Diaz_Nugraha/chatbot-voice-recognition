from bs4 import BeautifulSoup
import requests
import re

url = 'https://polindra.ac.id/akademik-dan-kemahasiswaan'
r = requests.get(url)
soup = BeautifulSoup(r.content, 'html.parser')

akademik = soup.find("li", {"class": "menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1204"})
ukms = akademik.select("ul > li > a")
for ukm in ukms[1:]:
    print(ukm.text)