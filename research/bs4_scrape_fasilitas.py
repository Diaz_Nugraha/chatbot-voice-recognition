from bs4 import BeautifulSoup
import requests
import re

def lab_scrape(url):

    url = url

    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')

    labList = []

    lab = soup.find_all("a", {"class": "elementor-accordion-title"})

    if not lab:
        lab = soup.find_all("a", {"class": "elementor-toggle-title"})    


    for tag in lab:
        labList.append(tag.text)


    print(';'.join(labList))
    # print(labList)


url = 'https://polindra.ac.id/fasilitas-2/'

r =  requests.get(url)

soup = BeautifulSoup(r.content, 'html.parser')

facilityUrlList = []

facility = soup.find_all("ul")[18]


for tag in facility:
    liTags = tag.select("li > a")    
    for tag in liTags:                
        facilityUrlList.append(tag['href'])
      

print(facilityUrlList[:4])

# print(facilityList)
for link in facilityUrlList[:4]:
    lab_scrape(link)


