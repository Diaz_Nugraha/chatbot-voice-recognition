from cmath import inf
from bs4 import BeautifulSoup
import requests
import re

url = 'https://pmb.polindra.ac.id/index.php/website/pmb/panduan'
r = requests.get(url)
soup = BeautifulSoup(r.content, 'html.parser')
info_titles = []
links = []
infos = soup.find_all("div", {"class": "kf_courses_des"})

for info in infos:
    title = info.find("h5").text
    link = info.find("a")["href"]
    info_titles.append(title)
    links.append(link)    

print(info_titles)
print(links)
    