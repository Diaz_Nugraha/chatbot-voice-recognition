
import sys
from bs4 import BeautifulSoup
import requests
def lab_scrape(url):

    url = url

    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')

    labList = []

    lab = soup.find_all("a", {"class": "elementor-accordion-title"})

    if not lab:
        lab = soup.find_all("a", {"class": "elementor-toggle-title"})

    for tag in lab:
        labList.append(tag.text)

    print(labList)

sys.modules[__name__] = lab_scrape

