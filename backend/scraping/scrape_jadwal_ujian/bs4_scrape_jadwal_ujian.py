from bs4 import BeautifulSoup
import requests


def scrape_jadwal_ujian():

    url = 'https://polindra.ac.id/akademik/'

    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')

    div = soup.find("div", {"id": "elementor-tab-content-1073"})
    
    p = div.select("div > p")

    jadwals = []
    has_child = len(p[0].find_all("a")) != 0
    
    if has_child == True:
        for a in p:
            b = a.find("a")['href']
            jadwals.append(b)

        return jadwals

    else:
        return None