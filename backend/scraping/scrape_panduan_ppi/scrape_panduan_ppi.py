from database.database_connect import connect_db
from scraping.scrape_panduan_ppi.bs4_scrape_panduan_ppi import scrape_panduan_ppi
from datetime import datetime

def scrapePanduanPpi():    
    connection = connect_db()    
    date = datetime.now()
    try:                
        data = scrape_panduan_ppi()        
        if(data is not None):
            for i in range(0, len(data), 1):  
                keterangan = data[i][2]       
                keterangan = keterangan[-4:]        
                connection[1].execute("SELECT EXISTS(SELECT * FROM informasi WHERE link='{}' AND status = '' AND id_kategori = 12)".format(data[i][1]))        
                result = connection[1].fetchall()        
                if(result[0][0] < 1):
                    connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(12, data[i][0], data[i][1], keterangan, '', date))
                    connection[0].commit()
                    print("berhasil masukkan data")
                else:
                    connection[1].execute("UPDATE informasi SET informasi = '{}', keterangan = '{}', updated_at = '{}' WHERE link = '{}' AND status = '' AND id_kategori = 12".format(data[i][0], keterangan,date, data[i][1]))
                    connection[0].commit()
                    print("berhasil update data")
                
            print("berhasil panduan ppi")   
            
        else: 
            connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(12, '', '', '', '', date))
            connection[0].commit()
            print("berhasil masukkan data")

        connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape panduan ppi', '{}', 'berhasil', '')".format(date))   
        connection[0].commit()        

        return "berhasil", date               

    except:
        connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape panduan ppi', '{}', 'gagal', '')".format(date))     
        connection[0].commit()                       
        print("runtime error panduan ppi")
        return "gagal", date