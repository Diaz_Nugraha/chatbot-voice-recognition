from database.database_connect import connect_db
from scraping.scrape_info_kampus.bs4_scrape_info_kampus import scrape_info_kampus
from datetime import datetime

def scrapeInfoKampus():
    connection = connect_db()
    date = datetime.now()
    try:
        data = scrape_info_kampus()
        if(data is not None):
            for i in range(0, len(data), 1):                
                connection[1].execute("SELECT EXISTS(SELECT * FROM informasi WHERE keterangan='{}' AND status = '' AND id_kategori = 2)".format(data[i][0]))
                result = connection[1].fetchall()
                if(result[0][0] < 1):
                    connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(2, data[i][1], data[i][2], data[i][0], '', date))
                    connection[0].commit()
                else:
                    connection[1].execute("UPDATE informasi SET informasi = '{}', updated_at = '{}' WHERE keterangan = '{}' AND status = '' AND id_kategori = 2".format(data[i][1], date, data[i][0]))
                    connection[0].commit()
                    print("berhasil update data")
            
            print("berhasil input info kampus")
        else:
            connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(2, '', '', '', '', date))
            connection[0].commit()
            print("berhasil masukkan data")
        
        connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape info kampus', '{}', 'berhasil', '')".format(date))   
        connection[0].commit()        

        return "berhasil", date

    except:
        connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape info kampus', '{}', 'gagal', '')".format(date))       
        connection[0].commit()        
        print("runtime error info kampus")
        return "gagal", date
