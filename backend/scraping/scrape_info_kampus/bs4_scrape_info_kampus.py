from bs4 import BeautifulSoup
import requests

headers = requests.utils.default_headers()
headers.update({
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
})

def scrape_info_kampus():
    url = 'https://polindra.ac.id/'

    r = requests.get(url, headers=headers)
    soup = BeautifulSoup(r.content, 'html.parser')

    tags = soup.find_all("div", {"class": "get-tuch text-left"})
    informations = []

    for tag in tags:
        title = tag.select("ul > li > h4")[0]        
        information = tag.select("ul > li > p")
        has_child = len(information[0].find_all("a")) != 0    
        if has_child == True:
            links = information[0].select("a")        
            link = links[0]['href']
            info = link.split(":")
            info = info[1]                
        else:
            info = information[0].text
            link = ''

        informations.append((title.text.lower(), info, link))    

    if len(informations) > 0:
        return informations
    else:
        return None
        

if __name__ == '__main__':
    print(scrape_info_kampus())
    

