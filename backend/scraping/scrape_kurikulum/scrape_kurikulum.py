from database.database_connect import connect_db
from scraping.scrape_kurikulum.bs4_scrape_kurikulum import scrape_kurikulum
from datetime import datetime

def scrapeKurikulum():    
    connection = connect_db()    
    date = datetime.now()    
    try:        
        ta = 0
        data = scrape_kurikulum()        
        if(data is not None):            
            for i in range(0, len(data), 1):                    
                connection[1].execute("SELECT EXISTS(SELECT * FROM informasi WHERE keterangan='{}' AND status = '' AND id_kategori = 10)".format(ta))        
                result = connection[1].fetchall()        
                if(result[0][0] < 1):
                    connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(10, '', data[i], ta, '', date))
                    connection[0].commit()           
                    print("berhasil masukkan data") 
                else:
                    connection[1].execute("UPDATE informasi SET link = '{}', updated_at = '{}' WHERE keterangan = '{}' AND status = '' AND id_kategori = 10".format(data[i], date, ta))
                    connection[0].commit()
                    print("berhasil update data")

                ta += 1
                
            print("berhasil kurikulum akademik")   
            
        else:             
            connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(10, '', '', '', '', date))
            connection[0].commit()
            print("berhasil masukkan data")

        connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape kurikulum', '{}', 'berhasil', '')".format(date))   
        connection[0].commit()        

        return "berhasil", date             

    except:
        connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape kurikulum', '{}', 'gagal', '')".format(date))   
        connection[0].commit()                       
        print("runtime error kurikulum")

        return "gagal", date