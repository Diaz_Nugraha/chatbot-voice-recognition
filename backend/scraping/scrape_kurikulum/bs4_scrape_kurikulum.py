from bs4 import BeautifulSoup
import requests


def scrape_kurikulum():

    url = 'https://polindra.ac.id/akademik/'

    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')

    div = soup.find("div", {"id": "elementor-tab-content-1074"})
    
    p = div.select("div > p")

    kurikulums = []
    has_child = len(p[0].find_all("a")) != 0
    
    if has_child == True:
        for a in p:
            b = a.find("a")['href']
            kurikulums.append(b)

        return kurikulums

    else:
        return None