from bs4 import BeautifulSoup
import requests
import re

headers = requests.utils.default_headers()
headers.update({
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
})

def lab_scrape(url):

    url = url

    r = requests.get(url, headers=headers)
    soup = BeautifulSoup(r.content, 'html.parser')

    labList = []

    lab = soup.find_all("a", {"class": "elementor-accordion-title"})

    if not lab:
        lab = soup.find_all("a", {"class": "elementor-toggle-title"})    


    for tag in lab:
        labList.append(tag.text)

    result = ';'.join(labList)
    return result    
    # print(labList)

def scrape_fasilitas():
    
    url = 'https://polindra.ac.id/fasilitas-2/'

    r =  requests.get(url, headers=headers)

    soup = BeautifulSoup(r.content, 'html.parser')

    facilityUrlList = []
    facilityNameList = []

    facility = soup.find_all("ul")[18]

    for tag in facility:
        liTags = tag.select("li > a")    
        for tag in liTags:                
            facilityUrlList.append(tag['href'])
            facilityNameList.append(tag.text)
            
    result = []
    # print(facilityList)
    for i in range(0, len(facilityNameList[:4]), 1):
        result.append((facilityNameList[i], facilityUrlList[i], lab_scrape(facilityUrlList[i])))

    if len(result) > 0:
        return result
    else:
        return None


if __name__ == '__main__':
    print(scrape_fasilitas())

