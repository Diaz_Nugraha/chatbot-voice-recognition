from database.database_connect import  connect_db
from scraping.scrape_fasilitas.bs4_scrape_fasilitas import scrape_fasilitas
from datetime import datetime


def scrapeFasilitas():
    
    connection = connect_db()
    date = datetime.now()
    try:
        data = scrape_fasilitas()
        if(data is not None):
            for i in range(0, len(data), 1):
                dataket = data[i][0].split(" ")
                dataket = dataket[2:]                
                keterangan = " ".join(dataket)                
                connection[1].execute("SELECT EXISTS(SELECT * FROM informasi WHERE keterangan='{}' AND status = '' AND id_kategori = 1)".format(keterangan))
                result = connection[1].fetchall()
                if(result[0][0] < 1):                            
                    connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(1, data[i][2], data[i][1], keterangan.lower(), '', date))
                    connection[0].commit()
                    print('berhasil masukkan data')
                else:
                    connection[1].execute("UPDATE informasi SET informasi = '{}', updated_at = '{}' WHERE keterangan = '{}' AND status = '' AND id_kategori = 1".format(data[i][2], date, keterangan))
                    connection[0].commit()
                    print("berhasil update data")

            print('berhasil input data fasilitas')
        else:
            connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(1, '', '', '', '', date))
            connection[0].commit()
            print("berhasil masukkan data")
        
        connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape fasilitas', '{}', 'berhasil', '')".format(date))   
        connection[0].commit() 
        return "berhasil", date

    except:
        connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape fasilitas', '{}', 'gagal', '')".format(date))    
        connection[0].commit()        
        print("runtime error fasilitas")
        return "gagal", date
