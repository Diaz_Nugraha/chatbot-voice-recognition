from database.database_connect import connect_db
from scraping.scrape_pengumuman.bs4_scrape_pengumuman import scrape_pengumuman
from datetime import datetime

def scrapePengumuman():
    connection = connect_db()
    date = datetime.now()
    try:
        data = scrape_pengumuman()        
        count = 0
        if data is not None:
            for i in range(0, len(data), 1):
                connection[1].execute("SELECT EXISTS(SELECT id FROM informasi WHERE keterangan='{}' AND status='' AND id_kategori=14)".format(data[i][2]))                
                result = connection[1].fetchall()                
                if(result[0][0] < 1):
                    connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(14, data[i][0], data[i][1],  data[i][2], '', date))
                    connection[0].commit()
                    print("berhasil masukkan data")
                else:
                    connection[1].execute("UPDATE informasi SET informasi = '{}', keterangan = '{}', updated_at = '{}' WHERE link = '{}'".format(data[i][0], data[i][2], date, data[i][1]))
                    connection[0].commit()
                    print("berhasil update data")

                count += 1
            
            print("berhasil input pengumuman")
        
        else:
            connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(14, '', '', '', '', date))
            connection[0].commit()
            print("berhasil masukkan data")

        connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape pengumuman', '{}', 'berhasil', '')".format(date))   
        connection[0].commit()         
        return "berhasil", date                 

    except:
        connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape pengumuman', '{}', 'gagal', '')".format(date))    
        connection[0].commit()                       
        print("runtime error pengumuman")
        return "gagal", date

if __name__ == '__main__':
    scrape_pengumuman()