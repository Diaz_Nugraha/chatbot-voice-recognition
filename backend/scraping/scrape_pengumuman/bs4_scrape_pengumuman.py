from bs4 import BeautifulSoup
import requests

def scrape_pengumuman():
    url = 'https://polindra.ac.id/category/pengumuman/'
    r = requests.get(url)

    soup = BeautifulSoup(r.content, 'html.parser')
    result = []
    wrappers = soup.find_all("div", {"ed-desc-wrap"})
    for i in wrappers:
        div = i.find("h3", {"class": "entry-title"})
        divdate = i.find("span", {"class": "posted-on"})

        link = div.select("a")[0]['href']
        title = div.select("a")[0].text
        date = divdate.select("a > time")[0].text
        datetime = divdate.select("a > time")[0]['datetime']
        info = [title, date]
        information = ";".join(info)
        result.append((information, link, datetime))
    
    if len(result) > 0:
        return result
    else:
        return None

if __name__ == '__main__':
    titles = scrape_pengumuman()
    for i in titles:
        print(i)    