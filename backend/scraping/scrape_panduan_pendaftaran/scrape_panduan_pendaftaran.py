from database.database_connect import connect_db
from scraping.scrape_panduan_pendaftaran.bs4_scrape_panduan_pendaftaran import scrape_panduan
from datetime import datetime

def scrapePanduanPendaftaran():
    connection = connect_db()
    date = datetime.now()
    try:
        data = scrape_panduan()
        if(data is not None):
            for i in range(0, len(data), 1):
                connection[1].execute("SELECT EXISTS(SELECT * FROM informasi WHERE link='{}' AND status = '' AND id_kategori = 11)".format(data[i][1]))
                result = connection[1].fetchall()
                if(result[0][0] < 1):
                    connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(11, data[i][0], data[i][1], data[i][0], '', date))
                    connection[0].commit()
                    print("berhasil masukkan data")
                else:
                    connection[1].execute("UPDATE informasi SET informasi = '{}', keterangan = '{}' updated_at = '{}' WHERE link = '{}' AND status = '' AND id_kategori = 11".format(data[i][0], data[i][0], date, data[i][1]))
                    connection[0].commit()
                    print("berhasil update data")

            print('berhasil input panduan pendaftaran')
        else:
            connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(11, '', '', '', '', date))
            connection[0].commit()
            print("berhasil masukkan data")

        connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape panduan pendaftaran', '{}', 'berhasil', '')".format(date))   
        connection[0].commit()           

        return "berhasil", date   

    except:
        connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape panduan pendaftaran', '{}', 'gagal', '')".format(date))   
        connection[0].commit()                       
        print("runtime error panduan")
        return "gagal", date
