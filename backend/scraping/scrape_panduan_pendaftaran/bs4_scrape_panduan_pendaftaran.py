from cmath import inf
from bs4 import BeautifulSoup
import requests
import re


def scrape_panduan():
    url = 'https://pmb.polindra.ac.id/index.php/website/pmb/panduan'
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    # info_titles = []
    # links = []
    informations = []
    infos = soup.find_all("div", {"class": "kf_courses_des"})

    for info in infos:
        title = info.find("h5").text
        link = info.find("a")["href"]
        informations.append((title, link))        
    
    if len(informations) > 0:
        return informations
    else:
        return None


if __name__ == '__main__':
    print(scrape_panduan())
    