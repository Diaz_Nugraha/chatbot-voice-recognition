from fileinput import filename
import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
from selenium.common.exceptions import NoSuchElementException
from database.database_connect import connect_db
from dotenv import load_dotenv
from datetime import datetime
import time, urllib.request
import requests

load_dotenv()

username = os.environ.get("IG_USERNAME")
password = os.environ.get("IG_PASSWORD")

# local_path = '/home/diaz/projects/tugas_akhir/chatbot-voice-recognition/backend'

def scrape_instagram():        
    local_path = os.getcwd()
    print(local_path)
    chrome_driver_path = Service('./scraping/scrape_instagram/chromedriver')
    driver = webdriver.Chrome(service=chrome_driver_path)
    # driver = webdriver.Chrome(executable_path='./chromedriver')
    driver.get("https://www.instagram.com/")
    element = WebDriverWait(driver, 25).until(
        EC.presence_of_element_located((By.NAME, "username"))
    )
    e = driver.find_element(By.NAME, "username")
    e.send_keys(username)
    e = driver.find_element(By.NAME, "password")
    e.send_keys(password)
    e = driver.find_element(By.XPATH, "//button[@type='submit']")
    e.click()
    element = WebDriverWait(driver, 25).until(
        EC.presence_of_all_elements_located((By.CLASS_NAME, "cmbtv"))        
    )
    e = driver.find_element(By.XPATH, "//button[@type='button']")
    e.click()
    element = WebDriverWait(driver, 30).until(
        EC.presence_of_all_elements_located((By.CLASS_NAME, "_a3gq"))
    )        
    time.sleep(5)
    e = driver.find_element(By.XPATH, "//button[contains(.,'Not Now')]")    
    e.click()

    time.sleep(5)
    driver.get('https://www.instagram.com/politekniknegeriindramayu/')
  
    time.sleep(5)
    posts = []
    subtitles = []
    date = datetime.now()
    status = "gagal"
    links = driver.find_elements(By.XPATH, "//a[@role='link']")
    for link in links:                
        post = link.get_attribute("href")
        if '/p/' in post:
            posts.append(post)
            
    # posts = posts[:3]
    for post in posts:
        time.sleep(5)
        driver.get(post)
        time.sleep(10)
        try:
            content = driver.find_element(By.XPATH, "//span[@class='_aacl _aaco _aacu _aacx _aad7 _aade']")
            subtitle = content.get_attribute("innerHTML").split("<br>")
            subtitle = "".join(subtitle).lower()       
            subtitle = subtitle.replace("'", " ") 
        except NoSuchElementException:
            subtitle = ''
            print('element doesnt exists')
        image = ''
        title = ''
        link = post.split("/")
        link = link[4]
        if 'lowongan pekerjaan' in subtitle:
            title = 3           
            image = scrape_information(driver)
        elif 'seminar' in subtitle:
            title = 4           
            image = scrape_information(driver)
        elif 'webinar' in subtitle:
            title = 4           
            image = scrape_information(driver)
        elif 'penerimaan' in subtitle:
            title = 5           
            image = scrape_information(driver)    
        else:
            title = 0
            image = 'None'            
        
        if title != 0:                                              
            image_name = f'image-{link}.jpg'            
            connection = connect_db()
            connection[1].execute("SELECT EXISTS(SELECT * FROM informasi WHERE keterangan='{}' AND status = '' AND id_kategori = {})".format(image_name, title))
            result = connection[1].fetchall()
            if result[0][0] < 1:                
                connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(title, subtitle, post, image_name, '', date))
                connection[0].commit()
                r = requests.get(image)                
                # filename = f'{local_path}/scraping/scrape_instagram/images/image-{link}.jpg'
                filename = f'{local_path}/assets/images/image-{link}.jpg'
                # filename = f'/images/image-{link}.jpg'
                with open(filename, 'wb') as fp:
                    fp.write(r.content)
            else: 
                connection[1].execute("UPDATE informasi SET informasi = '{}', updated_at = '{}' WHERE keterangan = '{}'  AND status = '' AND id_kategori = {}".format(subtitle, date, image_name, title))
                connection[0].commit() 

            connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape instagram', '{}', 'berhasil', '')".format(date))   
            connection[0].commit()        
            status = "berhasil"           

    return status, date

def scrape_information(driver):    
    image = driver.find_element(By.XPATH, "//img[@class='_aagt']")    
    image = image.get_attribute("src")

    return image


if __name__ == '__main__':
    images = scrape_instagram()
    for image in images:
        print(image)

