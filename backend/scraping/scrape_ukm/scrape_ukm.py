from database.database_connect import connect_db
from scraping.scrape_ukm.bs4_scrape_ukm import scrape_ukm
# from bs4_scrape_ukm import scrape_ukm
from datetime import datetime
from datetime import timedelta
import os
import mysql.connector


# def connect_db():
#     mydb = mysql.connector.connect(
#         host="localhost",
#         user="root",
#         password="root",
#         database="polindra_chatbot"
#     )

#     mycursor = mydb.cursor(buffered=True)
#     return mydb, mycursor

def scrapeUkm():
    connection = connect_db()
    date = datetime.now()
    try:        
        data = scrape_ukm()                
        if data is not None:                    
            for i in range(0, len(data), 1):                            
                connection[1].execute("SELECT EXISTS(SELECT id FROM informasi WHERE id_kategori=15 AND keterangan='{}' AND status='')".format(data[i][0]))
                result = connection[1].fetchall()                
                if(result[0][0] < 1):                    
                    connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(15, '', data[i][1], data[i][0], '', date))
                    connection[0].commit()
                    print("berhasil masukkan data")
                else:
                    connection[1].execute("UPDATE informasi SET link = '{}', updated_at = '{}' WHERE kid_kategori=15 AND keterangan='{}' AND status=''".format(data[i][1], date, data[i][0]))
                    connection[0].commit()
                    print("berhasil update data")
                
            print('berhasil input ukm')    

        else:
            connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ({}, '{}', '{}', '{}', '{}', '{}')".format(15, '', '', '', '', date))
            connection[0].commit()
            print("berhasil masukkan data")   

        connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape ukm', '{}', 'berhasil', '')".format(date))   
        connection[0].commit() 
        return "berhasil", date

    except:
        connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape ukm', '{}', 'gagal', '')".format(date))    
        connection[0].commit()
        print("runtime error ukm")
        return "gagal", date

if __name__ == '__main__':
    print(scrapeUkm()) 
