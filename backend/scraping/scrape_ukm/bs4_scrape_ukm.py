from bs4 import BeautifulSoup
import requests

# headers = requests.utils.default_headers()
# headers.update({
#     'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36',
# })
headers =  {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36',
}

def scrape_ukm():
    url = 'https://polindra.ac.id/akademik-dan-kemahasiswaan'
    r = requests.get(url, headers=headers)
    soup = BeautifulSoup(r.content, 'html.parser')

    akademik = soup.find("li", {"class": "menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1204"})
    ukms = akademik.select("ul > li > a")
    # return ukms
    result = []
    for ukm in ukms[1:]:
        result.append((ukm.text, ukm['href']))
    
    if len(result) > 0:
        return result
    else:
        return None

if __name__ == '__main__':
    print(scrape_ukm())