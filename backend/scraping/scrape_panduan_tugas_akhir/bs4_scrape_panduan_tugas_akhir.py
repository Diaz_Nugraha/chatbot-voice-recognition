from bs4 import BeautifulSoup
import requests


def scrape_panduan_tugas_akhir():

    url = 'https://polindra.ac.id/akademik/'

    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')

    div = soup.find("div", {"id": "elementor-tab-content-1079"})
    
    table = div.select("div > table > tbody")
    
    rows = table[0].find_all("tr")    
    
    result = []
    for row in rows:
        tanggal = row.find_all("td")[-1]
        tanggal = tanggal.text
        anchor = row.find("a")
        link = anchor['href']
        title = anchor.text
        result.append((title, link, tanggal))
        
    if len(result) > 0:
        return result
    else:
        return None
        
    
if __name__ == '__main__':
    print(scrape_panduan_tugas_akhir())
    # scrape_panduan_tugas_akhir()
    