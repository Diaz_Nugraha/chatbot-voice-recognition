from dotenv import load_dotenv
import os
import mysql.connector
from bs4 import BeautifulSoup
import requests
from datetime import datetime

def scrape_kalender():

    url = 'https://polindra.ac.id/akademik/'

    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')

    div = soup.find("div", {"class": "elementor-tab-content elementor-clearfix"})
    p = div.select("div > p")

    jadwals = []
    
    for a in p:
        b = a.find("a")['href']
        jadwals.append(b)

    if len(jadwals) > 0:
        return jadwals
    else:
        return None
        

if __name__ == '__main__':
    # print(scrape_kalender())
    scrape_kalender()
    # load_dotenv()

    # mydb = mysql.connector.connect(
    #     host=os.environ.get("DB_HOST"),
    #     user=os.environ.get("DB_USER"),
    #     password=os.environ.get("DB_PASS"),
    #     database=os.environ.get("DB_NAME")
    # )

    # mycursor = mydb.cursor()
    # data = scrape_kalender()
    # ta = 2013
    # for i in range(1, len(data), 1):
    #     date = datetime.now()        
    #     mycursor.execute("SELECT EXISTS(SELECT * FROM informasi_baru WHERE keterangan='{}')".format(ta))        
    #     result = mycursor.fetchall()        
    #     if(result[0][0] < 1):
    #         mycursor.execute("INSERT INTO informasi_baru (informasi, tipe_informasi, keterangan, updated_at) VALUES ('{}', '{}', '{}', '{}')".format(data[i], 'kalender_akademik', ta, date))
    #         mydb.commit()            
    #     else:
    #         print('data e ana isie')

    #     ta += 1

