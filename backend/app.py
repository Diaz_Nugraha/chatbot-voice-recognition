from crypt import methods
from datetime import datetime, timedelta
import os
from threading import Thread
import time
from flask import Flask, request, send_file, Response
from flask import jsonify
from flask_cors import CORS
from apscheduler.schedulers.background import BackgroundScheduler
from dotenv import load_dotenv
from database.database_connect import connect_db

from scraping.scrape_fasilitas.scrape_fasilitas import scrapeFasilitas
from scraping.scrape_kalender.scrape_kalender import scrapeKalender
from scraping.scrape_panduan_pendaftaran.scrape_panduan_pendaftaran import scrapePanduanPendaftaran
from scraping.scrape_pengumuman.scrape_pengumuman import scrapePengumuman
from scraping.scrape_ukm.scrape_ukm import scrapeUkm
from scraping.scrape_info_kampus.scrape_info_kampus import scrapeInfoKampus
from scraping.scrape_jadwal.scrape_jadwal import scrapeJadwal
from scraping.scrape_kurikulum.scrape_kurikulum import scrapeKurikulum
from scraping.scrape_jadwal_ujian.scrape_jadwal_ujian import scrapeJadwalUjian
from scraping.scrape_panduan_ppi.scrape_panduan_ppi import scrapePanduanPpi
from scraping.scrape_panduan_tugas_akhir.scrape_panduan_tugas_akhir import scrapePanduanTugasAkhir
from scraping.scrape_instagram.sel_scrape_instagram import scrape_instagram

# JWT
from flask_jwt_extended import create_access_token
from flask_jwt_extended import JWTManager
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_identity

# from flask_crontab import Crontab

load_dotenv()
app = Flask(__name__)

# crontab = Crontab(app)

cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

app.config['JWT_SECRET_KEY'] = os.environ.get('JWT_SECRET_KEY')
jwt = JWTManager(app)
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(hours=2)

connection = connect_db()

# scheduler = apscheduler.schedulers.background.BackgroundScheduler({'apscheduler.job_default.max_instances': 2})
scheduler = BackgroundScheduler(timezone="Asia/Jakarta")

@app.route('/api/admin-message', methods=['GET'])
def admin_message():
    connection = connect_db()    
    connection[1].execute('SELECT * FROM informasi WHERE id_kategori = 16 ORDER BY id DESC')
    result = connection[1].fetchone()    
    return jsonify({"data": result})
    
@app.route('/api/display-informations', methods=['GET'])
def display_informations():
    connection = connect_db()
    connection[1].execute('SELECT nama FROM kategori')
    result = connection[1].fetchall()
    arr = []
    for res in result:
        temp = res[0].replace("_", " ")
        arr.append(temp)

    return jsonify({"data": arr}), 200

@app.route('/api/login', methods=['POST'])
def login():
    connection = connect_db()
    email = request.form['email']
    password = request.form['password']      
    connection[1].execute('SELECT * FROM user WHERE email = %s',(email, ))
    result = connection[1].fetchall()    
    if result:
        if password == result[0][2]:
            access_token = create_access_token(identity=email)
            return jsonify(access_token = access_token)
        
        else:
            return jsonify({'password': 'Password yang anda masukkan salah!'}), 401    

    else:
        return jsonify({'email': 'Email yang anda masukkan salah!'}), 401    

@app.route('/api/dashboard', methods=['GET'])
@jwt_required()
def dashboard():
    current_user = get_jwt_identity()
    connection = connect_db()
    connection[1].execute("SELECT COUNT(*) FROM informasi WHERE status = 'approved'")
    informasi = connection[1].fetchall()
    connection[1].execute("SELECT COUNT(*) FROM informasi WHERE status = ''")
    informasi_baru = connection[1].fetchall()
    connection[1].execute("SELECT riwayat FROM scraping WHERE tipe='scheduled' ORDER BY id")
    date = connection[1].fetchone()
    date = date[0]
    date += timedelta(days=1)        
    percakapan = []
    connection[1].execute("SELECT id, nama FROM kategori")
    kategoris = connection[1].fetchall()
    for kategori in kategoris:        
        connection[1].execute("SELECT COUNT(*) FROM percakapan WHERE id_kategori='{}' AND tanggal BETWEEN date_sub(now(), INTERVAL 1 WEEK) and now()".format(kategori[0]))
        hasil = connection[1].fetchone()
        temp = (kategori[1], hasil[0])
        percakapan.append(temp)
        
    connection[1].execute("SELECT * FROM scraping ORDER BY id DESC LIMIT 10")
    result = connection[1].fetchall()
    arr = []
    for res in result:
        data = {
            "id": res[0],
            "informasi": res[1],
            "riwayat": res[2],
            "status": res[3]
        }
        arr.insert(0, data)

    connection[1].execute("SELECT kategori.nama, percakapan.pertanyaan, percakapan.status, percakapan.tanggal FROM percakapan LEFT JOIN kategori ON percakapan.id_kategori=kategori.id ORDER BY percakapan.id DESC LIMIT 10")
    conversations = connection[1].fetchall()
    arrconv = []
    for conv in conversations:
        data = {
            "kategori": conv[0],
            "pertanyaan": conv[1],
            "status": conv[2],
            "tanggal": conv[3]      ,

        }
        arrconv.insert(0, data)
    
    return jsonify({"data": arr, "informasi": informasi, "informasi_baru": informasi_baru, "next_scrape": date, "percakapan": percakapan, "conversations": arrconv}), 200


@app.route('/api/percakapan', methods=['GET'])
@jwt_required()
def percakapan():
    connection = connect_db()
    page = request.args.get("page", 1)
    newperpage = request.args.get("newPerPage", 1)

    offset = (int(page) - 1) * int(newperpage)
    limit = int(newperpage)
    
    connection[1].execute("SELECT COUNT(*) FROM percakapan")
    count = connection[1].fetchall()
    connection[1].execute("SELECT percakapan.id, percakapan.pertanyaan, kategori.nama, percakapan.status, percakapan.tanggal FROM percakapan LEFT JOIN kategori ON percakapan.id_kategori=kategori.id LIMIT {} OFFSET {}".format(limit, offset))
    result = connection[1].fetchall()
    arr = []
    for res in result:
        data = {
            "id": res[0],
            "pertanyaan": res[1],
            "kategori": res[2],
            "status": res[3],
            "tanggal": res[4]
        }
        arr.insert(0, data)

    return jsonify({"data": arr, "total": count[0][0]}), 200

@app.route('/api/information', methods=['GET'])
@jwt_required()
def information():
    connection = connect_db()
    page = request.args.get("page", 1)
    newperpage = request.args.get("newPerPage", 1)
    select_kat = request.args.get('selectKat', 1)

    offset = (int(page) - 1) * int(newperpage)
    limit = int(newperpage)

    connection[1].execute("SELECT * FROM kategori")
    kategori = connection[1].fetchall()

    if select_kat == 'all':
        connection[1].execute("SELECT COUNT(*) FROM informasi WHERE status = 'approved'")
        count = connection[1].fetchall()
        connection[1].execute("SELECT informasi.id, informasi.informasi, informasi.link, kategori.nama, informasi.keterangan FROM informasi LEFT JOIN kategori ON informasi.id_kategori=kategori.id  WHERE informasi.status = 'approved' LIMIT {} OFFSET {}".format(limit, offset))
        result = connection[1].fetchall()
        arr = []
        for res in result:
            data = {
                "id": res[0],
                "informasi": res[1],
                "link": res[2],
                "kategori": res[3],
                "keterangan": res[4]
            }
            arr.append(data)
            
        return jsonify({"data": arr, 'total': count[0][0], "kategori": kategori}), 200    
    else:
        connection[1].execute("SELECT COUNT(*) FROM informasi WHERE id_kategori={} AND status = 'approved'".format(select_kat))
        count = connection[1].fetchall()
        connection[1].execute("SELECT informasi.id, informasi.informasi, informasi.link, kategori.nama, informasi.keterangan FROM informasi LEFT JOIN kategori ON informasi.id_kategori=kategori.id  WHERE informasi.id_kategori = {} AND informasi.status = 'approved' LIMIT {} OFFSET {}".format(select_kat, limit, offset))
        result = connection[1].fetchall()

        arr = []
        for res in result:
            data = {
                "id": res[0],
                "informasi": res[1],
                "link": res[2],
                "kategori": res[3],
                "keterangan": res[4]
            }
            arr.append(data)
            
        return jsonify({"data": arr, 'total': count[0][0], "kategori": kategori}), 200    


@app.route('/api/show-information', methods=['GET'])
@jwt_required()
def show_information():
    id_information = request.args.get("id")
    connection = connect_db()
    
    connection[1].execute("SELECT EXISTS(SELECT * FROM informasi WHERE id='{}' AND status='approved')".format(id_information))
    cek = connection[1].fetchall()
    if(cek[0][0] > 0):
        connection[1].execute("SELECT informasi.id, informasi.informasi, informasi.link, kategori.nama, informasi.keterangan FROM informasi LEFT JOIN kategori ON informasi.id_kategori=kategori.id WHERE informasi.id='{}'".format(id_information))
        result = connection[1].fetchall()        
        for res in result:
            data = {
                "id": res[0],
                "informasi": res[1],
                "link": res[2],
                "kategori": res[3],
                "keterangan": res[4]
            }
        
        
        return jsonify({"data": data}), 200
    
    return jsonify({"msg": "data tidak tersedia"})

@app.route('/api/delete-information', methods=['DELETE'])
@jwt_required()
def delete_information():
    connection = connect_db()
    data = request.get_json()
    
    connection[1].execute("SELECT EXISTS(SELECT * FROM informasi WHERE id = '{}')".format(data['id_information']))
    cek = connection[1].fetchall()
    if(cek[0][0] > 0):
        connection[1].execute("DELETE FROM informasi WHERE id = '{}'".format(data['id_information']))
        connection[0].commit()

        return jsonify({'msg': "data berhasil dihapus"})

    return jsonify({"msg": "data tidak tersedia"})

@app.route('/api/new-information', methods=['GET'])
@jwt_required()
def new_information():    
    connection = connect_db()
    page = request.args.get('page', 1)    
    newperpage = request.args.get('newPerPage', 1)
    select_kat = request.args.get('selectKat', 1)
    
    offset = (int(page) - 1) * int(newperpage)
    limit = int(newperpage) 

    connection[1].execute("SELECT * FROM kategori")
    kategori = connection[1].fetchall()
    
    if select_kat == 'all':
        connection[1].execute("SELECT COUNT(*) FROM informasi WHERE status = ''")
        count = connection[1].fetchall()
        connection[1].execute("SELECT informasi.id, informasi.informasi, informasi.link, kategori.nama, informasi.keterangan FROM informasi LEFT JOIN kategori ON informasi.id_kategori=kategori.id WHERE informasi.status='' LIMIT {} OFFSET {}".format(limit, offset))
        result = connection[1].fetchall()
        arr = []
        for res in result:
            data = {
                "id": res[0],
                "informasi": res[1],
                "link": res[2],
                "kategori": res[3],
                "keterangan": res[4]
            }
            arr.append(data)
            
        return jsonify({"data": arr, 'total': count[0][0], "kategori": kategori}), 200    
    else:
        connection[1].execute("SELECT COUNT(*) FROM informasi WHERE id_kategori={} AND status = ''".format(select_kat))
        count = connection[1].fetchall()
        connection[1].execute("SELECT informasi.id, informasi.informasi, informasi.link, kategori.nama, informasi.keterangan FROM informasi LEFT JOIN kategori ON informasi.id_kategori=kategori.id WHERE informasi.id_kategori={} AND informasi.status='' LIMIT {} OFFSET {}".format(select_kat, limit, offset))
        result = connection[1].fetchall()        
        arr = []
        for res in result:
            data = {
                "id": res[0],
                "informasi": res[1],
                "link": res[2],
                "kategori": res[3],
                "keterangan": res[4]
            }
            arr.append(data)
            
        return jsonify({"data": arr, 'total': count[0][0], "kategori": kategori}), 200  

@app.route('/api/show-new-information')
@jwt_required()
def show_new_information():
    rev_id = request.args.get("id")
    connection = connect_db()

    connection[1].execute("SELECT EXISTS(SELECT * FROM informasi WHERE id='{}' AND status='')".format(rev_id))
    cek = connection[1].fetchall()
    if(cek[0][0] > 0):
        connection[1].execute("SELECT informasi.id, informasi.informasi, informasi.link, kategori.nama, informasi.keterangan FROM informasi LEFT JOIN kategori ON informasi.id_kategori=kategori.id WHERE informasi.id='{}'".format(rev_id))
        result = connection[1].fetchall()        
        for res in result:
            data = {
                "id": res[0],
                "informasi": res[1],
                "link": res[2],
                "kategori": res[3],
                "keterangan": res[4]
            }
        
        return jsonify({"data": data}), 200
    
    return jsonify({"msg": "data tidak tersedia"}), 404

@app.route('/api/accept-new-information', methods=['POST'])
@jwt_required()
def accept_new_information():        
    id_information = request.form['id']
    kategori = request.form['kategori']
    connection = connect_db()
    connection[1].execute("SELECT EXISTS(SELECT * FROM informasi WHERE id = '{}')".format(id_information))
    result = connection[1].fetchall()
    if(result[0][0] > 0):
        date = datetime.now()        
        connection[1].execute("SELECT id FROM kategori WHERE nama='{}'".format(kategori))
        id_tipe = connection[1].fetchall()
        new_data = {
            "id_kategori": id_tipe[0][0],
            "informasi": request.form['informasi'],
            "link": request.form['link'],            
            "keterangan": request.form['keterangan'],
            "status": 'approved',
            "updated_at": date
        }
        
        try:
            connection[1].execute("UPDATE informasi SET informasi = '{}', link = '{}', keterangan = '{}', status = '{}', updated_at = '{}' WHERE id = '{}'".format(new_data['informasi'], new_data['link'], new_data['keterangan'], new_data['status'], new_data['updated_at'], id_information))
            connection[0].commit()
            connection[1].execute("INSERT INTO informasi (id_kategori, informasi, link, keterangan, status, updated_at) VALUES ('{}', '{}', '{}', '{}', '{}', '{}')".format(16, "update " + kategori + " " + new_data['keterangan'], new_data['link'], "update " + kategori, 'approved', new_data['updated_at']))
            connection[0].commit()
            return jsonify({"msg": "informasi berhasil diperbarui"})
        except: 
            return jsonify({"msg": "gagal update informasi"}), 400                 

    else:
        return jsonify({"msg": "id informasi tidak ditemukan"}), 404

def scrapeData():    
    scrapeUkm()
    scrapePengumuman()
    scrapePanduanPendaftaran()
    scrapeKalender()
    scrapeFasilitas()
    scrapeInfoKampus()
    scrapeJadwal()
    scrapeKurikulum()
    scrapeJadwalUjian()
    scrapePanduanPpi()
    scrapePanduanTugasAkhir()
    scrape_instagram()
    connection = connect_db()
    date = datetime.now()
    connection[1].execute("INSERT INTO scraping (informasi, riwayat, status, tipe) VALUES ('scrape otomatis', '{}', 'berhasil', 'scheduled')".format(date))
    connection[0].commit()
    
def threadFunc(fn):
    thread  = Thread(target=fn)
    thread.start()    

def event_stream():
    result, date = scrapeUkm()        
    yield f"data: scrape ukm,{result},{date}  \n\n"        
    result2, date2 = scrapePengumuman()    
    yield f"data: scrape pengumuman,{result2},{date2}  \n\n"    
    result3, date3 =  scrapePanduanPendaftaran()    
    yield f"data: scrape panduan pendaftaran,{result3},{date3}  \n\n"    
    result4, date4 =  scrapeKalender()    
    yield f"data: scrape kalender,{result4},{date4}  \n\n"    
    result5, date5 =  scrapeFasilitas()    
    yield f"data: scrape fasilitas,{result5},{date5}  \n\n"    
    result6, date6 =  scrapeInfoKampus()    
    yield f"data: scrape info kampus,{result6},{date6}  \n\n"    
    result7, date7 =  scrapeJadwal()    
    yield f"data: scrape jadwal,{result7},{date7}  \n\n"    
    result8, date8 =  scrapeKurikulum()    
    yield f"data: scrape kurikulum,{result8},{date8}  \n\n"    
    result9, date9 =  scrapeJadwalUjian()    
    yield f"data: scrape jadwal ujian,{result9},{date9}  \n\n"    
    result10, date10 =  scrapePanduanPpi()    
    yield f"data: scrape panduan ppi,{result10},{date10}  \n\n"    
    result11, date11 =  scrapePanduanTugasAkhir()    
    yield f"data: scrape panduan tugas akhir,{result11},{date11}  \n\n"    
    result, date12 =  scrape_instagram()    
    yield f"data: scrape instagram,{result},{date12} \n\n"

@app.route("/api/data-stream")
def data_stream():
    return Response(event_stream(), mimetype="text/event-stream")
    

# @app.route("/api/scrape-data", methods=['GET', 'POST'])
# @jwt_required()
def scrape_data():
    threadFunc(scrapeData)        


@app.route("/image/<path:filename>")
def download_file(filename):
    return send_file(f'assets/images/{filename}', mimetype='image/gif')



# scheduler.add_job(id="scheduled Scrape UKM", func = scrapeUkm, trigger='cron', hour="23", minute="12")
# scheduler.add_job(id='scheduled Scrape Pengumuman', func=scrapePengumuman, trigger='cron', hour="22", minute="43")
# scheduler.add_job(id="scheduled Scrape Panduan", func=scrapePanduanPendaftaran, trigger='cron', hour="22", minute="45")
# scheduler.add_job(id="scheduled Scrape Kalender", func = scrapeKalender, trigger='cron', hour="23", minute="15")
# scheduler.add_job(id="scheduled Scrape Fasilitas", func=scrapeFasilitas, trigger='cron', hour="02", minute="21")
# scheduler.add_job(id='scheduled Scrape Info Kampus', func=scrapeInfoKampus, trigger='cron', hour="22", minute="53")
# scheduler.add_job(id="scheduled Scrape Jadwal", func = scrapeJadwal, trigger='cron', hour="22", minute="55")
# scheduler.add_job(id="scheduled Scrape Kurikulum", func = scrapeKurikulum, trigger='cron', hour="04", minute="59")
# scheduler.add_job(id="scheduled Scrape Jadwal Ujian", func = scrapeJadwalUjian, trigger='cron', hour="23", minute="00")
# scheduler.add_job(id="scheduled Scrape Panduan PPI", func = scrapePanduanPpi, trigger='cron', hour="23", minute="03")
# scheduler.add_job(id="scheduled Scrape Panduan Tugas Akhir", func = scrapePanduanTugasAkhir, trigger='cron', hour="23", minute="05")
# scheduler.add_job(id="scheduled Scrape Instagram", func=scrape_instagram, trigger='cron', hour="03", minute="27")
scheduler.add_job(id="scheduled Scrape Data", func=scrape_data, trigger='cron', hour="01", minute="30")
scheduler.start()

if __name__ == "__main__":
    app.run(port=5000, debug=True)
