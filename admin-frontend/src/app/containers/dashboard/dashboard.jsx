import axios from 'axios';
import { useEffect, useState } from 'react';
import Card from '../../components/dashboard/card/card';
import styles from './dashboard.module.css';
import LInfo from '../../../assets/informasi.png';
import LInfoB from '../../../assets/informasi_baru.png';
import LSched from '../../../assets/schedule.png';
import { Chart as ChartJS, CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend, } from 'chart.js';
import { Bar } from 'react-chartjs-2';

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
  );


const Dashboard = () => {
    const token = localStorage.getItem("token");
    const endpoint = process.env.REACT_APP_BACKEND_ENDPOINT;
    const [data, setData] = useState([]);            
    const [info, setInfo] = useState(0);
    const [infoBar, setInfoBar] = useState(0);
    const [sched, setSched] = useState("");
    const [conv, setConv] = useState([]);
    const [chartData, setChartData] = useState({
        labels: [""],
        datasets: [
        {
            label: 'Jumlah Data',
            data: 0,
            backgroundColor: "rgb(44, 122, 196)"
        }
        ],
    });
    
     

    const fetchData = async () => {        
        axios.get(`${endpoint}/dashboard`, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        }).then((res) => {
            setData(res.data.data);
            setInfo(res.data.informasi);
            setInfoBar(res.data.informasi_baru);                      
            setSched(res.data.next_scrape);
            setConv(res.data.conversations);
        
            setChartData({
                labels: res.data.percakapan?.map((con) => con[0]),              
                datasets: [
                {
                    label: 'Jumlah Data',
                    data: res.data.percakapan?.map((con) => con[1]),
                    backgroundColor: "rgba(53, 162, 235, 0.5)"
                }
                ],
            })                                
        })
        
    }

    const getRealTimeData = (result) => {
        const data = result.split(",")
        const newdata = {
            "informasi": data[0],
            "riwayat": data[2],
            "status": data[1]
        }        
        setData((old) => [...old, newdata])                        
    }    

    const fetchStream = () => {        
        const sse = new EventSource(`${endpoint}/data-stream`)
        sse.onmessage = e => getRealTimeData(String(e.data))
        sse.onerror = () => {
            sse.close()
        }
    }

    console.log(chartData)

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div className={styles.container}>
            <h1>Dashboard</h1>
            <div className={styles.cardcontainer}>
                <Card title="Information" content={info} img={LInfo} fontsize="23px" />                                
                <Card title="Informasi Baru" content={infoBar} img={LInfoB} fontsize="23px" />
                <Card title="Jadwal Scrape Selanjutnya" content={sched} img={LSched} fontsize="13px" />                
            </div>            
            <button className={styles.scrapeButton} onClick={fetchStream}>Manual Scrape</button>
            <div className={styles.tablecontainer}>
                <div className={styles.tablewrapper}>
                <h4 className={styles.tabletitle}>Tabel Scraping</h4>
                <table className={styles.table} border="1">           
                    <thead>
                        <tr>
                            <th className={styles.tableHead}>Informasi</th>
                            <th className={styles.tableHead}>riwayat</th>
                            <th className={styles.tableHead}>status</th>   
                        </tr>
                    </thead>     
                                    
                    <tbody>                    
                        {
                    data.map((d) => {
                            return <tr>
                                
                                <td className={styles.tableData}>{d.informasi}</td>
                                <td className={styles.tableData}>{d.riwayat}</td>
                                <td className={styles.tableData}>{d.status}</td>

                            </tr>                                                        
                        })             
                    }                                
                    </tbody>
                </table>           
                </div>
                {/* <div className={styles.tablewrapper}>
                <h4 className={styles.tabletitle}>Tabel Percakapan</h4>
                <table className={styles.table} border="1">           
                <thead>
                        <tr>
                            <th className={styles.tableHead}>Kategori Jawaban</th>
                            <th className={styles.tableHead}>Pertanyaan</th>
                            <th className={styles.tableHead}>Status</th>
                            <th className={styles.tableHead}>riwayat</th>                            
                        </tr>
                    </thead>     
                                    
                    <tbody>                    
                        {
                    conv.map((d) => {
                            return <tr>
                                
                                <td className={styles.tableData}>{d.kategori}</td>
                                <td className={styles.tableData}>{d.pertanyaan}</td>
                                <td className={styles.tableData}>{d.status}</td>
                                <td className={styles.tableData}>{d.tanggal}</td>                                

                            </tr>                                                        
                        })             
                    }                                
                    </tbody>
                </table>           
                </div> */}
                
            </div>
            
            
            <Bar
                data={chartData}
                options={{
                    plugins: {
                        title: {
                            display: true,
                            text: "Grafik percakapan 7 hari terakhir"
                        },
                        legend: {
                            display: true,
                            position: "top"
                        }
                    },
                    scales: {
                        xAxis: {
                            ticks: {
                                maxRotation: 90,
                                minRotation: 90
                            }
                        }
                    }
                }}
                
            />
            
        </div>
    );

}

export default Dashboard;