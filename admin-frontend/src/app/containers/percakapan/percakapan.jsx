import styles from './percakapan.module.css';
import DataTable from 'react-data-table-component';
import { useState } from 'react';
import { useEffect } from 'react';
import axios from 'axios';


const Percakapan = () => {

    const token = localStorage.getItem("token");
    const endpoint = process.env.REACT_APP_BACKEND_ENDPOINT;

    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [totalRows, setTotalRows] = useState(0);
    const [perPage, setPerPage] = useState(50);   
    
    const columns = [
        {
            name: "No",
            selector: row => data.findIndex((i) => i.id == row.id) + 1,
            sortable: true,
            width: "65px"           
        },
        {
            name: "Pertanyaan",
            selector: row => row.pertanyaan,
            sortable: false,
            width: "300px"            
        },
        {
            name: "Kategori Pertanyaan",
            selector: row => row.kategori,
            sortable: false,
            width: "200px"
        },
        {
            name: "Status",
            selector: row => row.status,
            sortable: true,
            width: "150px"
        }, 
        {
            name: "Waktu",
            selector: row => row.tanggal,
            sortable: false,
            width: "250px"
        }
    ];    

    const paginationComponentOptions = {
        rowsPerPageText: "Baris per halaman",
        rangeSeparatorText: "dari"
    }

    const handlePageChange = (page) => {
        fetchInformations(page);
    }

    const handlePerRowsChange = async (newPerPage, page) => {
        setLoading(true);
        const response = await axios.get(`${endpoint}/percakapan`, {
            params: {
                page: page,
                newPerPage: newPerPage
            },
            headers: {
                "Authorization": `Bearer ${token}`
            }
        });

        setData(response.data.data);
        setPerPage(newPerPage);
        setLoading(false);
    }


    const fetchInformations = async page => {
        setLoading(true);
        const response = await axios.get(`${endpoint}/percakapan`, {
            params: {
                page: page,
                newPerPage: perPage,                
            },
            headers: {
                "Authorization": `Bearer ${token}`
            }
        });
        setData(response.data.data)
        setTotalRows(response.data.total);
        setLoading(false);        
    }

    useEffect(() => {
        fetchInformations(1);
    }, [])

    return (
        <div className={styles.container}>
            <DataTable            
                title="Infomasi Percakapan"
                columns={columns}
                data={data}
                progressPending={loading}
                pagination
                paginationServer
                paginationTotalRows={totalRows}
                paginationRowsPerPageOptions={[50, 100]}
                onChangeRowsPerPage={handlePerRowsChange}
                onChangePage={handlePageChange}
                paginationComponentOptions={paginationComponentOptions}
            />
        </div>
    );

}

export default Percakapan;