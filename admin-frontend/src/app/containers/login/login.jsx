import styles from './login.module.css';
import background from '../../../assets/gedung.jpg';
import {Link, useNavigate} from 'react-router-dom';
import logo from '../../../assets/chatbot.png';


import { useEffect, useState } from 'react';
import axios from 'axios';

const Login = () => {

    const endpoint = process.env.REACT_APP_BACKEND_ENDPOINT;
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [validation, setValidation] = useState({});
    const navigate = useNavigate();

    const submitLogin = async (e) => {
        e.preventDefault();
        const formData = new FormData();

        formData.append('email', email);
        formData.append('password', password);

        await axios.post(`${endpoint}/login`, formData).then((res) => {
            localStorage.setItem('token', res.data.access_token);

            navigate('/dashboard');
        }).catch((err) => {
            console.log(err.response.data)
            setValidation(err.response.data)
        })

    }

    console.log(validation.msg);
    

    useEffect(() => {
        localStorage.getItem('token') != null && navigate('/dashboard');
    });

    return (
        <div className={styles.container}>
            <img  className={styles.image} src={background} />
            <div className={styles.content}>
                <div className={styles.loginbox}>
                    <div className={styles.inputcontainer}>
                        <div className={styles.titlecontainer}>
                            <h3 className={styles.title}>Polindra Chatbot</h3>
                            <img src={logo} width="50px" height="50px" />
                        </div>
                        
                        <form action="#" onSubmit={submitLogin}>
                        <div className={styles.input}>
                            <label htmlFor="email">Email</label>
                            <input type="text" name="email" id="email" className={styles.inputbox} onChange={(e) => setEmail(e.target.value)} />
                            {
                                validation.email && (
                                    <p style={{color: "red", marginBottom: "20px"}}>{validation.email}</p>
                                )
                            }
                            <label htmlFor="password">password</label>
                            <input type="password" name="password" id="password" className={styles.inputbox} onChange={(e) => setPassword(e.target.value)} />
                            {
                                validation.password && (
                                    <p style={{color: "red", marginBottom: "20px"}}>{validation.password}</p>
                                )
                            }                            
                        </div>
                        <div className={styles.buttoncontainer}>
                                {/* <Link to='/dashboard'> */}
                                    <button className={styles.button} value="submit">
                                        Login
                                    </button>
                                {/* </Link> */}
                        </div>
                        </form>
                    </div>                    
                </div>
            </div>
        </div>
    );

}

export default Login;