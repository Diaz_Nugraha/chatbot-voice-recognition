import styles from './home.module.css';
import background from '../../../assets/gedung.jpg';
import Sidebar from '../../components/home/sidebar/sidebar';
import Navbar from '../../components/home/navbar/navbar';
import Dashboard from '../dashboard/dashboard';
import Information from '../information/information';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import NewInformation from '../newinformation/newinformation';
import Profile from '../profile/profile';
import Review from '../review/review';
import View from '../view/view';
import Percakapan from '../percakapan/percakapan';

const Home = (props) => {
    
    const page = props.route;
    const navigate = useNavigate();

    useEffect(() => {
        localStorage.getItem('token') == null && navigate('/');
    });

    return (
        <div className={styles.container}>
            <img src={background} className={styles.image} />
            <div className={styles.content}>
                <div className={styles.contentcontainer}>                    

                    <Sidebar />                    
                    <Navbar />
                    <div className={styles.pagecontainer}>

                        <div className={styles.page}>
                            {
                                page == 'dashboard' && <Dashboard />
                            }
                            {
                                page == 'percakapan' && <Percakapan />
                            }
                            {
                                page == 'informasi' && <Information />
                            }
                            {
                                page == 'new-information' && <NewInformation />
                            }
                            {/* {
                                page == 'profile' && <Profile />
                            } */}
                            {
                                page == 'review' && <Review />
                            }
                            {
                                page == 'view' && <View />
                            }

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Home;
