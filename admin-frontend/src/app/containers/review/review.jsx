import axios from 'axios';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import styles from './review.module.css';

const Review = () => {

    const endpoint = process.env.REACT_APP_BACKEND_ENDPOINT;
    const { id } = useParams()
    const token = localStorage.getItem("token");
    const [kategori, setKategori] = useState("");    
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate()
    const [informasi, setInformasi] = useState("");
    const [keterangan, setKeterangan] = useState("");
    const [link, setLink] = useState("");
    const [fillable, setFillable] = useState();

    const fetchdata = async (e) => {
        setLoading(true);
        axios.get(`${endpoint}/show-new-information`, {
            params: {
                "id": id
            },
            headers: {
                "Authorization": `Bearer ${token}`
            }
        }).then((res) => {
            setLink(res.data.data.link);
            setInformasi(res.data.data.informasi);
            setKeterangan(res.data.data.keterangan);
            setKategori(res.data.data.kategori);
            (res.data.data.keterangan.length != 0) ? setFillable(false) : setFillable(true);        
            setLoading(false);
        }).catch((err) => {
            console.log(err);
            navigate('/new-information');
        })
        
    }    

    const backButton = () => {
        navigate("/new-information")
    }

    const submitForm = async (e) => {
        e.preventDefault()
        const formData = new FormData();

        formData.append('id', id);
        formData.append('informasi', informasi);
        formData.append('keterangan', keterangan);
        formData.append('link', link);
        formData.append('kategori', kategori); 
        
        axios.post(`${endpoint}/accept-new-information`, formData, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        }).then((res) => {            
            navigate('/information');
        }).catch((err) => {
            console.log(err);
            navigate('/new-information')
        })

    }

    const deleteInformation = async (e) => {
        e.preventDefault()
        axios.delete(`${endpoint}/delete-information`, {
            headers: {
                "Authorization": `Bearer ${token}`                
            },
            data: {
                "id_information": id
            }
        }).then((res) => {
            console.log(res.data);
            navigate('/new-information')            
        }).catch((err) => {
            console.log(err)
            navigate('/new-information')
        })
    }
    

    useEffect(() => {

        fetchdata()

    }, []);    
        

    return (
        <div className={styles.container}>
            <div className={styles.titleWrapper}>
                <h1>Review informasi baru {id}</h1>
                <button className={styles.buttonBack} onClick={backButton}>Kembali</button>
            </div>
            <form onSubmit={submitForm} action="#">

                <div className={styles.wrapper}>
                    <h3 className={styles.label}>Informasi</h3>                                                
                    <input type="text" name="informasi" id="informasi" className={styles.contentInformasi} value={informasi} required onChange={(e) => setInformasi(e.target.value)} />                      
                </div>
                <div className={styles.wrapper}>
                    <h3 className={styles.label}>Keterangan</h3>
                    {
                        fillable ?
                        <input type="text" name="keterangan" id="keterangan" className={styles.content} value={keterangan} onChange={(e) => setKeterangan(e.target.value)} />
                        :
                        <input type="text" name="keterangan" id="keterangan" className={styles.content} value={keterangan} readOnly />
                    }
                    
                </div>
                <div className={styles.wrapper}>
                    <h3 className={styles.label}>Link</h3>                                            
                    <input type="text" name="link" id="link" className={styles.content} value={link} onChange={(e) => setLink(e.target.value)} />                     
                </div>
                <div className={styles.wrapper}>
                    <h3 className={styles.label}>Kategori Informasi</h3>
                    <input type="text" name="kategori" id="kategori" className={styles.content} value={kategori} readOnly />
                </div>
            
            <div className={styles.buttons}>
                <button className={styles.buttonAccept} value="submit">Terima</button>
                <button className={styles.buttonReject} onClick={e => deleteInformation(e)}>Hapus</button>
            </div>
            </form>
        </div>
    );

}

export default Review;

