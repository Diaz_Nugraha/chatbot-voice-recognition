import axios from 'axios';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import styles from './view.module.css';

const View = () => {

    const endpoint = process.env.REACT_APP_BACKEND_ENDPOINT;
    const { id } = useParams()
    const token = localStorage.getItem("token");
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate()

    const fetchdata = async (e) => {
        setLoading(true);
        const response = await axios.get(`${endpoint}/show-information`, {
            params: {
                "id": id
            },
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
        
        setData(response.data.data);
        setLoading(false);
        
    }

    const backButton = () => {
        navigate("/information")
    }

    const deleteInformation = async (e) => {
        e.preventDefault();
        
        axios.delete(`${endpoint}/delete-information`, {
            headers: {
                "Authorization": `Bearer ${token}`
            },
            data: {
                "id_information": data.id
            }
            
        }).then((res) => {
            console.log(res.data);
            navigate('/information');
        }).catch((err) => {
            console.log(err);
            navigate('/information');
        })
    }

    useEffect(() => {

        fetchdata()

    }, []);

    console.log(data.informasi);

    return (
        <div className={styles.container}>
            <div className={styles.titleWrapper}>
                <h1>Detail Informasi {id}</h1>
                <button className={styles.buttonBack} onClick={backButton}>Kembali</button>
            </div>

            <div className={styles.wrapper}>
                <h3 className={styles.label}>Informasi</h3>
                <div className={styles.contentInformasi}>{data.informasi}</div>
            </div>
            <div className={styles.wrapper}>
                <h3 className={styles.label}>Keterangan</h3>
                <div className={styles.content}>{data.keterangan}</div>
            </div>
            <div className={styles.wrapper}>
                <h3 className={styles.label}>Link</h3>
                <div className={styles.content}>{data.link} </div>
            </div>
            <div className={styles.wrapper}>
                <h3 className={styles.label}>Kategori Informasi</h3>
                <div className={styles.content}>{data.kategori}</div>
            </div>
            <div className={styles.buttons}>                
                <button className={styles.buttonReject} onClick={e => deleteInformation(e)}>Hapus</button>
            </div>
        </div>
    );
}

export default View;