import styles from './profile.module.css';

const Profile = () => {

    return (
        <div className={styles.container}>
            <h1>Profile</h1>
            <div className={styles.wrapper}>
                <div className={styles.card}>
                    <div className={styles.icon}></div>
                    <label htmlFor="" style={{marginTop: "30px"}}>Name</label>                
                    <input type="text" value="diaz" readOnly className={styles.input} /> 
                    <label htmlFor="">Name</label>                
                    <input type="email" value="diaznugraha@email.com" readOnly className={styles.input} /> 
                    <label htmlFor="">Password</label>                
                    <input type="password" value="password" readOnly className={styles.input} /> 
                </div>                
            </div>
        </div>
    );

}

export default Profile;