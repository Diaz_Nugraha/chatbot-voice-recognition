import axios from "axios";
import { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import { Link, useNavigate } from "react-router-dom";
import styles from './newinformation.module.css';



const NewInformation = () => {


    const token = localStorage.getItem("token");   
    const endpoint = process.env.REACT_APP_BACKEND_ENDPOINT; 

    const [data, setData] = useState([]);
	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
    const [kategori, setKategori] = useState([]);
    const [selectKat, setSelectKat] = useState("all");
    const navigate = useNavigate();

    const columns = [
        {
            name: "No",
            selector: row => data.findIndex((i) => i.id == row.id) + 1,
            sortable: true,
            width: "60px"
        },
        {
            name: "informasi",
            selector: row => row.informasi,
            sortable: true,
            width: "300px"
        },
        {
            name: "link",
            selector: row => row.link,
            sortable: true,
            width: "100px"
        },
        {
            name: "kategori",
            selector: row => row.kategori,
            sortable: true,
            width: "200px"
        },
        {
            name: "keterangan",
            selector: row => row.keterangan,
            sortable: true,
            width: "200px"
        },
        {
            name: "action",
            button: true,
            cell: row => (
               
                    <a href={"http://localhost:3000/review/" + row.id} onClick={e => redirectPage(row.id, e)} target="_blank" style={{ padding: "10px 10px", "backgroundColor": "blue", "color": "white", "fontWeight": "bold", "cursor": "pointer" }} >Review</a>

            ),
        }
    ]

    const redirectPage = (id, e) => {
        e.preventDefault();
        navigate('/review/' + id);
    }


	const fetchInformations = async page => {
		setLoading(true);

        const response = await axios.get(`${endpoint}/new-information`, {
            params: {
                page: page,
                newPerPage: perPage,
                selectKat: selectKat
            },
            headers: {
                "Authorization": `Bearer ${token}`
            }
        });                

		setData(response.data.data);
		setTotalRows(response.data.total);
		setLoading(false);
        setKategori(response.data.kategori);    
	};

	const handlePageChange = page => {
		fetchInformations(page);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		setLoading(true);

        const response = await axios.get(`${endpoint}/new-information`, {
            params: {
                page: page,
                newPerPage: newPerPage
            },
            headers: {
                "Authorization": `Bearer ${token}`
            }
        });        

		setData(response.data.data);
		setPerPage(newPerPage);
		setLoading(false);
        
	};

    useEffect(() => {
		fetchInformations(1); // fetch page 1 of users
		
	}, [selectKat]);    

    return (
        <div className={styles.container}>
            <h1>Informasi Baru</h1>
            <label htmlFor="kategori">Pilih Kategori: </label>
            <select name="kategori" id="kategori" defaultValue={'all'} onChange={(e) => setSelectKat(e.target.value)}>
                <option value="all">Semua</option>
                {
                    kategori.map((kat) => {
                        return (                            
                            <option value={kat[0]} key={kat[0]}>{kat[1]}</option>
                        )
                    })
                }                
            </select>
            <div className={styles.contentcontainer}>
            <DataTable
                title="Daftar Informasi Baru"
                columns={columns}
                data={data}
                progressPending={loading}
                pagination
                paginationServer
                paginationTotalRows={totalRows}
                onChangeRowsPerPage={handlePerRowsChange}
                onChangePage={handlePageChange}
            />
        </div>
    </div>
    );

}

export default NewInformation;