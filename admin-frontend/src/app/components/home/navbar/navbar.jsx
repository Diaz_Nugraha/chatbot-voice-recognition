import jwtDecode from 'jwt-decode';
import styles from './navbar.module.css';

const Navbar = () => {

    const token = localStorage.getItem('token');

    const decoded = jwtDecode(token);
    const name = decoded.sub;

    return (
        <div className={styles.container}>
            <div className={styles.name}>{name}</div>
        </div>
    );

}

export default Navbar;