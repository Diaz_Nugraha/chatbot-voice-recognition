import styles from './sidebar.module.css';
import logo from '../../../../assets/chatbot.png';
import { Link, useNavigate } from 'react-router-dom';

const Sidebar = () => {

    const navigate = useNavigate();

    const logout = async (e) => {
        e.preventDefault();
        localStorage.removeItem('token');
        navigate('/');
    }

    return (
        <div className={styles.container}>
            <div className={styles.wrapper}>
                <div className={styles.iconcontainer}>
                    <div className={styles.logo}>
                        <img src={logo} width="50px" height="50px" />
                        <h3 className={styles.logoTitle} >Polindra Chatbot</h3>
                    </div>
                </div>
                <div className={styles.menucontainer}>
                    <Link to="/dashboard" style={{textDecoration: "none"}}>
                        <div className={styles.menu}>
                            <p className={styles.titlemenu}> Dashboard</p>
                        </div>
                    </Link>
                    <Link to="/percakapan" style={{textDecoration: "none"}}>
                        <div className={styles.menu}>
                            <p className={styles.titlemenu}> Percakapan</p>
                        </div>
                    </Link>
                    <Link to="/information" style={{textDecoration: "none"}}>
                        <div className={styles.menu}>
                            <p className={styles.titlemenu}> Informasi</p>
                        </div>
                    </Link>
                    <Link to="/new-information" style={{textDecoration: "none"}}>
                        <div className={styles.menu}>
                            <p className={styles.titlemenu}> Informasi Baru</p>
                        </div>
                    </Link>
                    {/* <Link to="/profile" style={{textDecoration: "none"}}>
                        <div className={styles.menu}>
                            <p className={styles.titlemenu}> Profile</p>
                        </div>                    
                    </Link> */}
                    
                    <div className={styles.menu} onClick={logout}>
                        <p className={styles.titlemenu}> Logout</p>
                    </div>                                        
                </div>
            </div>
        </div>
    );

}

export default Sidebar;