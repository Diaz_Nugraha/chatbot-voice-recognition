import styles from './card.module.css';

const Card = (props) => {

    return (
        <div className={styles.container}>
            <div className={styles.titlecontainer}>
                <h3 className={styles.title}>
                    {props.title}
                </h3>
            </div>
            <div className={styles.contentcontainer}>
                <div className={styles.icon}>
                    <img src={props.img} width="40px" height="40px" style={{color: "white"}} />
                </div>
                <div className={styles.content} style={{ fontSize: props.fontsize }}>
                    {props.content}
                </div>
            </div>
        </div>
    );

}

export default Card;