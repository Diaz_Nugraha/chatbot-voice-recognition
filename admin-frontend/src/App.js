import Login from "./app/containers/login/login";
import { Route, BrowserRouter as Router, Routes, Redirect, Navigate, useLocation } from 'react-router-dom';
import Home from "./app/containers/home/home";


function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" exact element={<Login />}></Route>
        <Route path="/dashboard" exact element={<Home route="dashboard" />}></Route>
        <Route path="/percakapan" exact element={<Home route="percakapan" />}></Route>
        <Route path="/information" exact element={<Home route="informasi" />}></Route>
        <Route path="/new-information" exact element={<Home route="new-information" />}></Route>
        <Route path="/profile" exact element={<Home route="profile" />}></Route>
        <Route path="/review/:id" exact element={<Home route="review" />}></Route>
        <Route path="/view/:id" exact element={<Home route="view" />}></Route>
      </Routes>
    </Router>    
  );
}

export default App;
