import os
import mysql.connector
from dotenv import load_dotenv

load_dotenv()

def connect_db():
    mydb = mysql.connector.connect(
        host=os.environ.get("DB_HOST"),
        user=os.environ.get("DB_USER"),
        password=os.environ.get("DB_PASS"),
        database=os.environ.get("DB_NAME")
    )

    mycursor = mydb.cursor()
    return mydb, mycursor