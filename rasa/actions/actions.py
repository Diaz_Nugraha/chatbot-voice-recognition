# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, EventType
from rasa_sdk.types import DomainDict
import os

from database.database_connect import connect_db
from datetime import datetime

PATH = os.environ.get("BACKEND_ENDPOINT")

# LOKASI
class ActionTanyaLokasi(Action):

    def name(self) -> Text:
        return "action_tanya_lokasi"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()        
        intent = 'contact address'
        connection[1].execute("SELECT informasi FROM informasi WHERE keterangan =%s AND status = 'approved'", (intent, ))
        result = connection[1].fetchall()    
        date = datetime.now()
        pertanyaan = tracker.latest_message['text']
        
        if len(result) < 1:
            dispatcher.utter_message(text='Maaf informasi tidak tersedia')    
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (2, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit()
        else:
            dispatcher.utter_message(text="Ini informasi lokasi polindra saat ini")
            dispatcher.utter_message(text="Lokasinya ada di {}".format(result[0][0]))        
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (2, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit()

        return []

class ActionGetInformasiContactAddress(Action):
    def name(self) -> Text:
        return "action_get_informasi_contact_address"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()
        connection[1].execute("SELECT * FROM informasi WHERE keterangan='phone number' OR keterangan='email address' AND status = 'approved'")
        informations = connection[1].fetchall()
        pertanyaan = tracker.latest_message['text']
        date = datetime.now()
        if len(informations) < 1:
            dispatcher.utter_message(text='Maaf informasi tidak tersedia')
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (2, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit()
        else:
            dispatcher.utter_message(text="Kamu dapat menghubungi kampus untuk informasi lebih lanjut melalui nomor telepon ini\n{}".format(informations[0][2]))
            dispatcher.utter_message(text="Atau melalui alamat email kami di {}".format(informations[1][2]))                        
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (2, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit()
        
        return []

# JURUSAN
class ActionSebutJurusan(Action):
    def name(self) -> Text:
        return "action_sebut_jurusan"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()
        connection[1].execute("SELECT DISTINCT keterangan FROM informasi where id_kategori = 1 AND status = 'approved'")
        jurusans = connection[1].fetchall()
        pertanyaan = tracker.latest_message['text']
        date = datetime.now()
        if len(jurusans) < 1:
            dispatcher.utter_message(text='Maaf informasi tidak tersedia')   
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (1, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit()
        else:
            dispatcher.utter_message(text="Ini adalah jurusan-jurusan yang ada di polindra")
            for i, jurusan in enumerate(jurusans, start=1):
                dispatcher.utter_message(text="jurusan {}".format(jurusan[0]))            
            
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (1, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit()
        
        
        return []

class ValidateInformasiJurusanForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_informasi_jurusan_form"
    
    def validate_inform_jurusan(self, slot_value: Any, dispatcher: CollectingDispatcher,
                                tracker: Tracker,
                                domain: DomainDict) -> Dict[Text, Any]:
        jurusan = slot_value
        if len(jurusan) == 0:
            dispatcher.utter_message(text="Maaf mohon diulangi")
            return {"inform_jurusan": None}
        
        return {"inform_jurusan": jurusan}

class ActionGetInformasi(Action):
    def name(self) -> Text:
        return "action_get_informasi"
    
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        jurusan = tracker.get_slot("inform_jurusan")                
        connection = connect_db()
        connection[1].execute("SELECT informasi FROM informasi WHERE keterangan LIKE '%{}' AND status = 'approved'".format(jurusan.lower(), jurusan.lower()))
        result = connection[1].fetchone()
        if result is None:
            dispatcher.utter_message(text="Maaf jurusan tidak dikenali")
        if len(result[0]) < 1:
            dispatcher.utter_message(text='Maaf informasi tidak tersedia')        
        else:                            
            labs = result[0].split(";")
            dispatcher.utter_message(text="Ini adalah fasilitas yang ada di {} Politeknik Negeri Indramayu".format(jurusan))
            for lab in labs:
                dispatcher.utter_message(text="{}".format(lab))

        return [SlotSet("inform_jurusan", None)]

# KALENDER AKADEMIK
class ActionPilihTahunKalender(Action):
    def name(self) -> Text:
        return "action_pilih_tahun_kalender"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()
        connection[1].execute("SELECT keterangan FROM informasi WHERE id_kategori=9 AND status = 'approved'")
        kalenders = connection[1].fetchall()
        pertanyaan = tracker.latest_message['text']
        date = datetime.now()
        if len(kalenders) < 1:
            dispatcher.utter_message(text='Maaf informasi tidak tersedia')
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (9, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit()

        else:
            dispatcher.utter_message(text='Ini adalah daftar kalender akademik yang tersedia')
            for i, kalender in enumerate(kalenders, start=1):
                dispatcher.utter_message(text="Tahun awal ajaran {}".format(kalender[0]))

            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (9, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit()  

        
        return []

class ValidateInformasiKalenderForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_informasi_kalender_form"

    def validate_inform_tahun(self, slot_value: Any, dispatcher: CollectingDispatcher,
                                tracker: Tracker,
                                domain: DomainDict) -> Dict[Text, Any]:
        tahun = slot_value
        if len(tahun) == 0:
            dispatcher.utter_message(text="Maaf mohon diulangi")
            return {"inform_tahun": None}
        return {"inform_tahun": tahun}

class ActionGetInformasiKalender(Action):
    def name(self) -> Text:
        return "action_get_informasi_kalender"
    
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        tahun_kalender = tracker.get_slot("inform_tahun")    
        tahun_kalender = int(tahun_kalender)    
        connection = connect_db()
        connection[1].execute("SELECT link FROM informasi WHERE keterangan = '{}' AND status = 'approved'".format(tahun_kalender))
        result = connection[1].fetchall()
        if len(result) < 1:
            dispatcher.utter_message(text='Maaf informasi tidak tersedia')
        else:
            dispatcher.utter_message(text='Kalender akademik akademik tahun ajaran {}/{} bisa dilihat disini: '.format(tahun_kalender, tahun_kalender + 1))
            resp = {
                "type": "link",
                "payload": result[0][0]
            }
            dispatcher.utter_message(attachment=resp)

        return [SlotSet("inform_tahun", None)]

# UNIT KEGIATAN MAHASISWA
class ActionGetInformasiUKM(Action):
    def name(self) -> Text:
        return "action_get_informasi_ukm"
    
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()
        connection[1].execute("SELECT informasi, link FROM informasi WHERE id_kategori=15 AND status = 'approved'")
        ukms = connection[1].fetchall()
        pertanyaan = tracker.latest_message['text']
        date = datetime.now()
        if len(ukms) < 1:
            dispatcher.utter_message(text='Maaf informasi tidak tersedia')
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (15, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit()
        else:
            dispatcher.utter_message(text="Ini adalah daftar unit kegiatan mahasiswa yang ada di Politeknik Negeri Indramayu")
            for ukm in ukms:
                dispatcher.utter_message(text="{} \nUntuk informasi lebih lanjut bisa kunjungi di bawah ini:".format(ukm[0]))
                resp = {
                    "type": "link",
                    "payload": ukm[1]
                }
                dispatcher.utter_message(attachment=resp)

            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (15, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit()  

        date = datetime.now()
        
        return []

# PANDUAN PENDAFTARAN
class ActionGetInformasiPanduanPendaftaran(Action):
    def name(self) -> Text:
        return "action_get_informasi_panduan_pendaftaran"
    
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()
        connection[1].execute("SELECT informasi, link FROM informasi WHERE id_kategori=11 AND status = 'approved'")
        panduans = connection[1].fetchall()
        pertanyaan = tracker.latest_message['text']
        date = datetime.now()
        if len(panduans) < 1:
            dispatcher.utter_message(text='Maaf informasi tidak tersedia')
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (11, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit()  
        else:
            dispatcher.utter_message(text="Ini adalah panduan pendaftaran Politeknik Negeri Indramayu")
            for panduan in panduans:
                dispatcher.utter_message(text="Panduan Tentang {} \nBisa dilihat di sini:".format(panduan[0]))                
                resp = {
                    "type": "link",
                    "payload": panduan[1]
                }
                dispatcher.utter_message(attachment=resp)
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (11, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit()  

    
        return []

# PENGUMUMAN WEB POLINDRA
class ActionGetInformasiPengumumanWeb(Action):
    def name(self) -> Text:
        return "action_get_informasi_pengumuman_web"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()
        connection[1].execute("SELECT informasi, link, keterangan FROM informasi WHERE id_kategori=14 AND status = 'approved'")
        pengumumans = connection[1].fetchall()
        pertanyaan = tracker.latest_message['text']
        date = datetime.now()
        if len(pengumumans) < 1:
            dispatcher.utter_message(text='Maaf informasi tidak tersedia')
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (14, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit()  
        else:
            dispatcher.utter_message(text="Ini informasi pengumuman yang ada pada website Politeknik Negeri Indramayu")
            for pengumuman in pengumumans:           
                caption = pengumuman[0].split(";")
                tanggal = caption[1]
                caption = caption[0]
                if len(caption) > 50:
                    caption = caption[:50] + "..."
                dispatcher.utter_message(text="{} \nBisa dilihat melalui link dibawah ini:".format(caption))
                dispatcher.utter_message(text="Tanggal pengumuman {}".format(tanggal))
                resp = {
                    "type": "link",
                    "payload": pengumuman[1]
                }
                dispatcher.utter_message(attachment=resp)

            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (14, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit()  

       
        return []

# PANDUAN PPI
class ActionGetInformasiPanduanPpi(Action):
    def name(self) -> Text:
        return "action_get_informasi_panduan_ppi"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()
        connection[1].execute("SELECT informasi, link, keterangan FROM informasi WHERE id_kategori=12 AND status = 'approved'")
        informations = connection[1].fetchall()
        pertanyaan = tracker.latest_message['text']
        date = datetime.now()
        if len(informations) < 1:
            dispatcher.utter_message(text='Maaf informasi tidak tersedia')
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (12, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit()  
        else:
            dispatcher.utter_message(text="Ini adalah daftar panduan ppi atau magang yang disediakan Politeknik Negeri Indramayu")
            for info in informations:
                dispatcher.utter_message(text="{} untuk tahun {}".format(info[0], info[2])) 
                dispatcher.utter_message(text="Bisa dilihat melalui link dibawah ini")
                resp = {
                    "type": "link",
                    "payload": info[1]
                }
                dispatcher.utter_message(attachment=resp)

            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (12, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit()  

        
        return []

# PANDUAN TUGAS AKHIR
class ActionPilihTahunPanduanTa(Action):
    def name(self) -> Text:
        return "action_pilih_tahun_panduan_ta"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()
        connection[1].execute("SELECT DISTINCT keterangan FROM informasi WHERE id_kategori=13 AND status = 'approved'")
        informations = connection[1].fetchall()
        pertanyaan = tracker.latest_message['text']
        date = datetime.now()
        if len(informations) < 1:
            dispatcher.utter_message(text='Maaf informasi tidak tersedia')
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (13, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit() 
        else:
            dispatcher.utter_message(text='Ini adalah daftar panduan tugas akhir Politeknik Negeri Indramayu yang tersedia')
            for info in informations:
                dispatcher.utter_message(text='Keterangan Tanggal {}'.format(info[0]))

            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (13, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit() 

  
        return []                

class ValidateInformasiPanduanTaForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_informasi_panduan_ta_form"

    def validate_inform_tahun(self, slot_value: Any, dispatcher: CollectingDispatcher,
                            tracker: Tracker,
                            domain: DomainDict) -> Dict[Text, Any]:
        tahun = slot_value
        if len(tahun) == 0:
            dispatcher.utter_message(text="Maaf mohon diulangi")
            return {"inform_tahun": None}
        return {"inform_tahun": tahun}
        

class ActionGetInformasiPanduanTa(Action):
    def name(self) -> Text:
        return "action_get_informasi_panduan_ta"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        tahun_ta = tracker.get_slot("inform_tahun")        
        connection = connect_db()
        connection[1].execute("SELECT informasi, link, keterangan FROM informasi WHERE keterangan='{}' AND id_kategori=13 AND status = 'approved'".format(tahun_ta))
        informations = connection[1].fetchall()
        if len(informations) < 1:
            dispatcher.utter_message(text="Maaf informasi tidak tersedia")
        else:
            dispatcher.utter_message(text="Berikut panduan tugas akhir polindra tahun {}".format(tahun_ta))
            for info in informations:
                dispatcher.utter_message(text="{} \nKeterangan tanggal: {}".format(info[0], info[2]))
                dispatcher.utter_message(text="Bisa dilihat melalui link dibawah ini")
                resp = {
                    "type": "link",
                    "payload": info[1]
                }
                dispatcher.utter_message(attachment=resp)
        
        return [SlotSet("inform_tahun", None)]

# INFORMASI KURIKULUM
class ActionGetInformasiKurikulum(Action):
    def name(self) -> Text:
        return "action_get_informasi_kurikulum"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()
        connection[1].execute("SELECT informasi, link, keterangan FROM informasi WHERE id_kategori=10 AND status = 'approved'")
        informations = connection[1].fetchall()
        pertanyaan = tracker.latest_message['text']
        date = datetime.now()
        if len(informations) < 1:
            dispatcher.utter_message(text="Maaf informasi tidak tersedia")
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (10, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit() 
        else:
            for info in informations:
                dispatcher.utter_message(text="{}  \nKeterangan: {}".format(info[0], info[2]))
                dispatcher.utter_message(text="Bisa dilihat melalui link dibawah ini")
                resp = {
                    "type": "link",
                    "payload": info[1]
                }
                dispatcher.utter_message(attachment=resp)
            
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (10, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit() 

        
        return []

class ActionGetInformasiJadwal(Action):
    def name(self) -> Text:
        return "action_get_informasi_jadwal"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()
        connection[1].execute("SELECT informasi, link, keterangan FROM informasi WHERE id_kategori=7 AND status = 'approved'")
        informations = connection[1].fetchall()
        pertanyaan = tracker.latest_message['text']
        date = datetime.now()
        if len(informations) < 1:
            dispatcher.utter_message(text="Maaf informasi tidak tersedia")
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (7, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit() 
        else:
            for info in informations:
                dispatcher.utter_message(text="{} \nKeterangan: {}".format(info[0], info[2]))
                dispatcher.utter_message(text="Bisa dilihat melalui link dibawah ini")
                resp = {
                    "type": "link",
                    "payload": info[1]
                }
                dispatcher.utter_message(attachment=resp)
            
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (7, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit() 

 
        return []

class ActionGetInformasiJadwalUjian(Action):
    def name(self) -> Text:
        return "action_get_informasi_jadwal_ujian"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()
        connection[1].execute("SELECT informasi, link, keterangan FROM informasi WHERE id_kategori=8 AND status = 'approved'")
        informations = connection[1].fetchall()
        pertanyaan = tracker.latest_message['text']
        date = datetime.now()
        if len(informations) < 1:
            dispatcher.utter_message(text="Maaf informasi tidak tersedia")
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (8, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit() 
        else:
            for info in informations:
                dispatcher.utter_message(text="{} \nKeterangan: {}".format(info[0], info[2]))
                dispatcher.utter_message(text="Bisa dilihat melalui link dibawah ini")
                resp = {
                    "type": "link",
                    "payload": info[1]
                }
                dispatcher.utter_message(attachment=resp)
            
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (8, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit() 

     
        return []

class ActionGetInformasiLowonganKerja(Action):
    def name(self) -> Text:
        return "action_get_informasi_lowongan_kerja"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()
        connection[1].execute("SELECT informasi, link, keterangan FROM informasi WHERE id_kategori=3 AND status = 'approved'")
        informations = connection[1].fetchall()
        pertanyaan = tracker.latest_message['text']
        date = datetime.now()
        if len(informations) < 1:
            dispatcher.utter_message(text="Maaf informasi tidak tersedia")
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (3, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit() 
        else:
            dispatcher.utter_message(text="Ini adalah informasi lowongan kerja yang tersedia")
            for info in informations:
                dispatcher.utter_message(text="{}".format(info[0]))                                
                dispatcher.utter_message(image=f"{PATH}/image/{info[2]}")                 
                dispatcher.utter_message(text="Link selengkapnya di bawah ini")
                resp = {
                    "type": "link",
                    "payload": info[1]
                }
                dispatcher.utter_message(attachment=resp)

            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (3, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit() 


        return []

class ActionGetInformasiSeminar(Action):
    def name(self) -> Text:
        return "action_get_informasi_seminar"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()
        connection[1].execute("SELECT informasi, link, keterangan FROM informasi WHERE id_kategori=4 AND status = 'approved'")
        informations = connection[1].fetchall()
        pertanyaan = tracker.latest_message['text']
        date = datetime.now()
        if len(informations) < 1:
            dispatcher.utter_message(text='Maaf informasi tidak tersedia')
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (4, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit() 
        else:
            dispatcher.utter_message(text="Ini adalah informasi seminar yang tersedia")
            for info in informations:
                caption = info[0]
                if len(caption) > 100:
                    caption = caption[:100] + "..."
                dispatcher.utter_message(text="{}".format(caption))
                dispatcher.utter_message(image=f"{PATH}/image/{info[2]}")    
                dispatcher.utter_message(text="Link selengkapnya di bawah ini")
                resp = {
                    "type": "link",
                    "payload": info[1]
                }
                dispatcher.utter_message(attachment=resp)         

            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (4, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit()     


        return []
                
class ActionGetInformasiPenerimaan(Action):
    def name(self) -> Text:
        return "action_get_informasi_penerimaan"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        connection = connect_db()
        connection[1].execute("SELECT informasi, link, keterangan FROM informasi WHERE id_kategori=5 AND status = 'approved'")
        informations = connection[1].fetchall()
        pertanyaan = tracker.latest_message['text']
        date = datetime.now()
        if len(informations) < 1:
            dispatcher.utter_message(text='Maaf informasi tidak tersedia')
            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (5, '{}', 'data tidak ada', '{}')".format(pertanyaan, date))
            connection[0].commit()   
        else:
            dispatcher.utter_message(text="Ini adalah informasi penerimaan mahasiswa baru yang tersedia")
            for info in informations:
                dispatcher.utter_message(text="{}".format(info[0]))
                dispatcher.utter_message(image=f"{PATH}/image/{info[2]}")  
                dispatcher.utter_message(text="Bisa dilihat melalui link dibawah ini")
                resp = {
                    "type": "link",
                    "payload": info[1]
                }
                dispatcher.utter_message(attachment=resp)

            connection[1].execute("INSERT INTO percakapan (id_kategori, pertanyaan, status, tanggal) VALUES (5, '{}', 'berhasil', '{}')".format(pertanyaan, date))
            connection[0].commit()   
        
      
        return []


def clean_name(name):
    return "".join(c for c in name if c.isalpha())
